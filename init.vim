" neovim custom configuration

set nocompatible
syn on
set title
autocmd BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal g`\"" | endif
set bg=dark
ca Wq wq
ca WQ wq

set shortmess+=I
set vb t_vb=""
set fileformat=unix
"set notextauto
set smartindent

" set tabstop=4
" " set shiftwidth=4

set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2
set smarttab

" Show trailing tabs and spaces (|---)
" match CursorLine /^\s\+/
" this here causes problems:
"set listchars=tab:¦-,trail:\ ,extends:#
set list

" autocmd BufEnter * let &titlestring = "vim: " . hostname() . " (" . expand("%:t") . ")"
autocmd BufEnter * let &titlestring = "VIM " . expand("%:t") . " [" . expand($USER) . "@" . hostname() . "]"
" autocmd BufEnter * let &titlestring = "VIM " . expand("%t") . " [" . expand($USER) . "@" . hostname() . "]"

" set mouse=a
set mouse=

" leave insert mode quickly
if ! has('gui_running')
  set ttimeoutlen=10
  augroup FastEscape
    autocmd!
    au InsertEnter * set timeoutlen=0
    au InsertLeave * set timeoutlen=1000
  augroup END
endif

"set noshowmode


set nobackup
set noswapfile
set writebackup
set undolevels=9001
set history=9001
set autoread
set noautochdir
set encoding=utf-8
set fileformat=unix

set background=dark
set autoindent
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
set wrap
set linebreak
set backspace=indent,eol,start
set hlsearch
set noincsearch
set ignorecase
set smartcase
set number
set t_Co=256

map <F2> <Esc>:call FontAndBGToggle()<cr>
function! FontAndBGToggle()
    if &background=='dark'
        set background=light
    else
        set background=dark
    endif
endfunction

set nonumber
set noshowmode

"colorscheme deus
"colorscheme pablo
" colorscheme nordlicht
"colorscheme srcery
"colorscheme monokai
colorscheme nordlicht
"let g:airline_theme='deus'
"let g:airline_theme='base16_google'
" let g:airline_theme='base16_mocha'
" let g:airline_theme='aurora'
let g:airline_theme='jellybeans'
"let g:airline_theme='srcery'
" let g:airline_theme='badcat'
" let g:airline_theme='wombat'
" let g:airline_theme='atomic'
" let g:airline_theme='base16_bright'
" let g:airline_theme='base16_classic'
" let g:airline_theme='base16_ocean'
" let g:airline_theme='base16_chalk'
" let g:airline_theme='onedark'
" let g:airline_theme='base16_eighties'
" let g:airline_theme='base16_ashes'
" let g:airline_theme='ayu_dark'
" let g:airline_theme='term'
" let g:airline_theme='base16_atelierheath'
" let g:airline_theme='base16_embers'
" let g:airline_theme='base16_flat'
" let g:airline_theme='base16_snazzy'
" let g:airline_theme='base16_atelierforest'
" let g:airline_theme='murmur'
" let g:airline_theme='base16_greenscreen'
" let g:airline_theme='soda'
" let g:airline_theme='hybridline'
" let g:airline_theme='seagull'
" let g:airline_theme='ravenpower'
" let g:airline_theme='base16_3024'
" let g:airline_theme='cobalt2'
" let g:airline_theme='desertink'
" let g:airline_theme='base16_hopscotch'
" let g:airline_theme='base16_grayscale'
" let g:airline_theme='ayu_light'
" let g:airline_theme='base16_vim'
" let g:airline_theme='base16_isotope'
" let g:airline_theme='base16_oceanicnext'
" let g:airline_theme='dark_minimal'
" let g:airline_theme='sol'
" let g:airline_theme='badwolf'
" let g:airline_theme='dark'
" let g:airline_theme='base16_atelierdune'
" let g:airline_theme='papercolor'
" let g:airline_theme='minimalist'
" let g:airline_theme='distinguished'
" let g:airline_theme='base16_twilight'
" let g:airline_theme='base16_bespin'
" let g:airline_theme='laederon'
" let g:airline_theme='base16_nord'
" let g:airline_theme='base16_gruvbox_dark_hard'
" let g:airline_theme='base16_marrakesh'
" let g:airline_theme='fruit_punch'
" let g:airline_theme='base16_railscasts'
" let g:airline_theme='term_light'
" let g:airline_theme='base16_atelierlakeside'
" let g:airline_theme='fairyfloss'
" let g:airline_theme='solarized'
" let g:airline_theme='base16_apathy'
" let g:airline_theme='base16_brewer'
" let g:airline_theme='base16_shapeshifter'
" let g:airline_theme='base16_default'
" let g:airline_theme='durant'
" let g:airline_theme='peaksea'
" let g:airline_theme='night_owl'
" let g:airline_theme='kalisi'
" let g:airline_theme='zenburn'
" let g:airline_theme='solarized_flood'
" let g:airline_theme='bubblegum'
" let g:airline_theme='powerlineish'
" let g:airline_theme='dracula'
" let g:airline_theme='base16_spacemacs'
" let g:airline_theme='base16_monokai'
" let g:airline_theme='qwq'
" let g:airline_theme='vice'
" let g:airline_theme='alduin'
" let g:airline_theme='xtermlight'
" let g:airline_theme='base16_colors'
" let g:airline_theme='base16_londontube'
" let g:airline_theme='base16_tomorrow'
" let g:airline_theme='base16color'
" let g:airline_theme='cool'
" let g:airline_theme='ouo'
" let g:airline_theme='base16_atelierseaside'
" let g:airline_theme='sierra'
" let g:airline_theme='molokai'
" let g:airline_theme='serene'
" let g:airline_theme='ayu_mirage'
" let g:airline_theme='silver'
" let g:airline_theme='hybrid'
" let g:airline_theme='tomorrow'
" let g:airline_theme='base16'
" let g:airline_theme='base16_solarized'
" let g:airline_theme='jet'
" let g:airline_theme='ubaryd'
" let g:airline_theme='base16_summerfruit'
" let g:airline_theme='deus'
" let g:airline_theme='raven'
" let g:airline_theme='violet'
" let g:airline_theme='understated'
" let g:airline_theme='simple'
" let g:airline_theme='behelit'
" let g:airline_theme='luna'
" let g:airline_theme='base16_harmonic16'
" let g:airline_theme='base16_shell'
" let g:airline_theme='monochrome'
" let g:airline_theme='base16_adwaita'
" let g:airline_theme='base16_paraiso'
" let g:airline_theme='base16_seti'
" let g:airline_theme='base16_pop'
" let g:airline_theme='lucius'
" let g:airline_theme='biogoo'
" let g:airline_theme='kolor'
" let g:airline_theme='base16_codeschool'
" let g:airline_theme='light'
" let g:airline_theme='angr'
" let g:airline_powerline_fonts=0

set showtabline=1
"hi TabLineFill ctermfg=238 ctermbg=DarkGreen
"hi TabLine ctermfg=Blue ctermbg=Yellow
" hi TabLineSel ctermfg=238 ctermbg=Green
"hi Title ctermfg=LightBlue ctermbg=Magenta
"hi TabLineFill guifg=gray guibg=DarkGreen ctermfg=239 ctermbg=DarkGreen
set shortmess=I

if $COLORTERM == ''
  colorscheme ron
endif

set nofoldenable

if &diff
set nofoldenable
endif

" add timestamp <<<
function! InsertTimeStamp()
   call complete(col('.'), [strftime("%Y-%m-%d"), strftime("%Y-%m-%dT%H:%M"), strftime("%d. %B %Y"), strftime("%H:%M")])
   return ''
endfunction " >>>

inoremap äT <C-R>=InsertTimeStamp()<CR>

" disable cursor shaping
let $NVIM_TUI_ENABLE_CURSOR_SHAPE = 0
set guicursor=

" disable cursor blinking
set guicursor=a:blinkon0

" airline tab line settings
let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' '
"let g:airline#extensions#tabline#left_alt_sep = '|'

" airline path formatter for tabs
"let g:airline#extensions#tabline#formatter = 'unique_tail'

" powerline symbols ftw!
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

let g:airline_skip_empty_sections = 1

"let g:airline_right_alt_sep = '|'
"let g:airline_left_sep = '⮀'
"let g:airline_left_alt_sep = '⮁'
"let g:airline_right_sep = '\'
"let g:airline_right_alt_sep = '⮃'
"let g:airline_symbols.branch = '⭠'
"let g:airline_symbols.readonly = '⭤'
"let g:airline_symbols.linenr = '⭡'
" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.colnr = ' :'
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ' :'
let g:airline_symbols.maxlinenr = '☰ '
let g:airline_symbols.dirty=' ⚡'
let g:airline_symbols.notexists = ' ⁝'

let g:airline#extensions#hunks#enabled=1
let g:airline#extensions#branch#enabled=1

let g:airline#extensions#tabline#buffer_nr_show = 0

let g:airline#extensions#whitespace#enabled = 1
let g:airline#extensions#whitespace#symbol = '!'

nnoremap <C-m> :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

set runtimepath+=~/.config/nvim/autoload/nerdtree



